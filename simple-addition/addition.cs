﻿using System;

namespace nkrs14
{
    namespace simple_addition
    {
        class Program
        {
            static void Main()
            {
                int a, b;
                String s;
                Console.WriteLine("Geben Sie den ersten Summanden ein!");
                s = Console.ReadLine();
                a = Int32.Parse(s);

                Console.WriteLine("Geben Sie den zweiten Summanden ein!");
                s = Console.ReadLine();
                b = Int32.Parse(s);

				Console.WriteLine("Das Maximum von " + a + " und " + b + " beträgt " + Mathe1.Max(a, b));
				Console.WriteLine("Das Minimum von " + a + " und " + b + " beträgt " + Mathe1.Min(a, b));
            }
        }

		class Mathe1 {
			public static int Min(int x, int y ) {
				return x > y ? y : x; 
			}

			public static int Max( int x, int y ) {
				return x > y ? x : y;
			}
		}
	}
}