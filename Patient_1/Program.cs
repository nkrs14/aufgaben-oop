﻿using System;

namespace nkrs14.Patient_1 {
	class Programm_Patient_1 {
		static void Main( string[] args ) {
			Console.WriteLine("Program started");

			Patient patient_1 = new Patient("Max Mustermann",120);

			patient_1.Röntgenuntersuchung(5.7);
			patient_1.Röntgenuntersuchung(65d);

			Patient patient_2 = new Patient("Benedikt Beispiel");

			patient_2.Röntgenuntersuchung(7.5);
			patient_2.Röntgenuntersuchung(12.25);

			if ( patient_1 > patient_2 ) {
				Console.WriteLine("Der kleinere Patient hat die Nummer " + patient_2.Patientennummer);
			} else {
				Console.WriteLine("Der kleinere Patient hat die Nummer " + patient_1.Patientennummer);
			}

		}
	}
}
