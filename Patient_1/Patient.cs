﻿using System;

namespace nkrs14.Patient_1 {
	public class Patient {
		//All members

		/// <summary>
		/// Der Name des patienten.
		/// </summary>
		private string name;

		/// <summary>
		/// Die Anzahl der durchgeführten Untersuchungen
		/// </summary>
		private long anzahl_untersuchungen;

		/// <summary>
		/// Die gesamte Strahlungsdosis in mSv, die der Patient aushalten musste. 
		/// </summary>
		private double gesamtdosis;

		/// <summary>
		/// Die körpergröße in cm
		/// </summary>
		private double körpergröße;

		/// <summary>
		/// Die Patientennummer (wird automatisch zugewiesen)
		/// </summary>
		private long patientennummer;

		/// <summary>
		/// Die gesamte anzahl der patienten.
		/// </summary>
		static private long anzahl_patienten;


		//All the getters
		public string Name { get => name; }
		public long Patientennummer { get => patientennummer; }
		public double Körpergröße { get => körpergröße; }

		static public long Anzahl_patienten { get => anzahl_patienten; }

		//The list of constructors

		/// <summary>
		/// Erstellt einen neuen Patienten mit dem Namen "Patient" und einer Körpergröße von 180cm
		/// </summary>
		public Patient() : this("Patient") {
		}

		/// <summary>
		/// Erstellt einen neuen Patienten mit gegebenen Namen und einer Körpergröße von 180cm
		/// </summary>
		/// <param name="patient_name">Patient name.</param>
		public Patient( string patient_name ) : this(patient_name, 180) {

		}

		/// <summary>
		/// Erstellt einen neuen Patienten mit gegebenen Namen und Körpergröße
		/// </summary>
		/// <param name="patient_name">Der Name des patienten</param>
		/// <param name="größe">die körpergröße des patienten in cm</param>
		public Patient( string patient_name, double größe ) {
			name = patient_name;
			anzahl_untersuchungen = 0;
			gesamtdosis = 0;
			körpergröße = größe;

			anzahl_patienten++;
			patientennummer = anzahl_patienten;
			Console.WriteLine("Patient {0} , mit dem name " + Name + " erstellt.\n", Anzahl_patienten);
		}

		/// <summary>
		/// Verringert die gesamtzahl der patienten um 1
		/// Releases unmanaged resources and performs other cleanup operations before the
		/// <see cref="T:nkrs14.Patient_1.Patient"/> is reclaimed by garbage collection.
		/// </summary>
		~Patient() {
			anzahl_patienten--;
		}

		//add dosis and print total dosis

		/// <summary>
		/// Führt eine Röntgenuntersuchung mit gegebener Strahlungsdosis durch.
		/// </summary>
		/// <param name="dosis">Strahlungsdosis in mSv.</param>
		public void Röntgenuntersuchung( double dosis ) {
			anzahl_untersuchungen++;
			gesamtdosis += dosis;

			Console.WriteLine("Röntgenuntersuchung Nr. " + anzahl_untersuchungen + " von " + name + ".");
			Console.WriteLine("Gesamtdosis nach der Untersuchung: " + gesamtdosis + " mSv\n");

		}

		//copare the height of two patients
		public static bool operator >( Patient a, Patient b ) {
			Console.WriteLine(a.Name + ((a.Körpergröße > b.Körpergröße) ? " ist größer als " : " ist kleiner oder genauso groß wie " ) + b.Name);
			return a.Körpergröße > b.Körpergröße;
		}

		public static bool operator <( Patient a, Patient b ) {
			Console.WriteLine(a.Name + ( ( a.Körpergröße < b.Körpergröße ) ? " ist kleiner als " : " ist größer oder genauso groß wie " ) + b.Name);
			return a.Körpergröße < b.Körpergröße;
		}

		public static bool operator <=( Patient a, Patient b ) {
			Console.WriteLine(a.Name + ( ( a.Körpergröße > b.Körpergröße ) ? " ist größer als " : " ist kleiner oder genauso groß wie " ) + b.Name);
			return a.Körpergröße <= b.Körpergröße;
		}

		public static bool operator >=( Patient a, Patient b ) {
			Console.WriteLine(a.Name + ( ( a.Körpergröße < b.Körpergröße ) ? " ist kleiner als " : " ist größer oder genauso groß wie " ) + b.Name);
			return a.Körpergröße >= b.Körpergröße;
		}
	}
}