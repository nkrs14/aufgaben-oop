﻿using System;

namespace nkrs14 {
	namespace Cake {
		class Cake {
			static void Main() {

				Console.Title = "Cake maker";
				Console.WriteLine("Welcome to the cake maker.");

				double cake_weight = 0;
				int number_of_ingredients = 3;
				int number_of_slices = 0;

				Console.Write("How many ingredients does your cake have? ");
				number_of_ingredients = Int32.Parse(Console.ReadLine());
				Ingredient[] ingredients = new Ingredient[number_of_ingredients];

				for(int i = 0; i < ingredients.Length; i++) {
					ingredients[i] = new Ingredient();
					ingredients[i].Init(i);
					cake_weight += ingredients[i].Get();
				}

				Console.Write("\nHow many slices should the cake have? ");
				number_of_slices =  Int32.Parse(Console.ReadLine());

				Console.WriteLine("\nRecepie:");

				for ( int i = 0; i < ingredients.Length; i++ ) {
					ingredients[i].Print();
				}

				Console.WriteLine("\nThe total mass of the cake is " + cake_weight + " grams.");
				Console.WriteLine("The mass of each slice is " + cake_weight / number_of_slices + " grams.");

			}
		}
	}
}
