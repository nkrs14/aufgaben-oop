﻿using System;

namespace nkrs14 {
	namespace Cake {
		public class Ingredient {
			private string name;
			private double amount;
			private bool is_initilized = false;

			public Ingredient() { }

			public void Init( int ingredient_index ) {
				Console.Write("\nWhat is ingredient " + (ingredient_index+1) + " called? ");
				name = Console.ReadLine();

				Console.Write("How much " + name + " do you want to use (in grams)? ");
				amount = Double.Parse(Console.ReadLine());

				is_initilized = true;
			}

			public void Print() {
				if ( is_initilized ) {
					Console.WriteLine(amount + " grams of " + name + " is needed");
				}
			}

			public double Get() {
				return amount;
			}
		}
	}
}
