﻿using System;

namespace nkrs14.Firma_1 {
	class Program {
		static void Main( string[] args ) {
			var arbeiter = new Mitarbeiter();
			var lohnarbeiter = new Lohnempfaenger();
			var verkauf = new Verkaeufer();
			var man = new Manager();

			var Belegschaft = new Mitarbeiter[4];
			Belegschaft[0] = arbeiter;
			Belegschaft[1] = lohnarbeiter;
			Belegschaft[2] = verkauf;
			Belegschaft[3] = man;

			foreach (var m in Belegschaft ) {
				Console.WriteLine(m.get_name);
			}

			Console.WriteLine(( (Lohnempfaenger) Belegschaft[1] ).get_wage());
		}
	}

	class Mitarbeiter {
		private String name;

		public Mitarbeiter() : this("Max Mitarbeiter") { }

		public Mitarbeiter( String name ) {
			this.name = name; 
		}

		public String get_name {
			get => name;
		} 
	}

	class Lohnempfaenger : Mitarbeiter {
		private int lohn;
		private int stunden; 

		public Lohnempfaenger( String name ) : base(name) { }
		public Lohnempfaenger():base("Ludwig Lohnempfänger"){ }

		public void set_wage( int n ) {
			lohn = n;
		}

		public void set_hour( int n ) {
			stunden = n;
		}

		public int get_wage() {
			return lohn * stunden;
		}
	}

	class Verkaeufer : Lohnempfaenger {
		private int comission;

		public Verkaeufer( String name ) : base(name) { }
		public Verkaeufer():base("Vladimir Verkäufer"){ }

		public void set_comission( int n ) {
			comission = n; 
		}

		public int get_comission() {
			return comission;
		}
	}

	class Manager : Mitarbeiter {
		private int gehalt;

		public Manager( String name ) : base(name) {}
		public Manager():base("Manfred Manager") {}

		public void set_gehalt( int n ) {
			gehalt = n;
		}

		public int get_wage() {
			return gehalt;
		}

	}

}
