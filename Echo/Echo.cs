﻿using System;

namespace nkrs14 {
	namespace Echo {
		class Echo {
			public static void Main() {
				Console.Write("Enter something that will be echoed:");
				string s = Console.ReadLine();
				Console.WriteLine(s);
			}
		}
	}
}
