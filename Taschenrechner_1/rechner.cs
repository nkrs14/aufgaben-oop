﻿using System;

namespace nkrs14.Taschenrechner_1 {
	class Taschenrechner_1 {
		static void Main() {
			double Wert = 0d;
			double operand = 0d;

			while ( true ) {
				Console.WriteLine("Der aktuelle Wert beträgt " + Wert + ".\n");

				int rechenart = AuswahlDerRechenart();
				if(rechenart == 0 ) {
					break;
				}

				try {
					operand = AuswahlDesOperanden();
				} catch (Exception){
					Console.WriteLine("Abbruch\n");
					continue;
				}

				switch ( rechenart ) {
					case ( 1 ):
						Wert += operand;
						break;

					case ( 2 ):
						Wert -= operand;
						break;

					case ( 3 ):
						Wert /= operand;
						break;

					case ( 4 ):
						Wert *= operand;
						break;

					default:
						throw new Exception("Eine unerreichbare Stelle wurde ausgeführt, das sollte nicht passieren.");	
				}//switch

			}//while

			Console.WriteLine("Programm wird beendet");


		}//Main

		/// <summary>
		/// Gibt informationen zur Hilfe im Ungang mit dem Rechner aus.
		/// </summary>
		static void HilfeAusgeben() {
			Console.WriteLine("\nHilfestellungen:\n");
			Console.WriteLine("Zuerst muss eine der Grundrechenarten +,-,/,* ausgewählt werden.");
			Console.WriteLine("Alternativ kann hier auch e zum beenden des Programms oder h für die Hilfe eingegeben werden.\n");
			Console.WriteLine("Nun muss eine Zahl eingegeben werden, die mit dem Wert verrechnet wird.");
			Console.WriteLine("Fall man aus Versehen die Fahlsche operation gewählt hat kann man mit x auch eine andere Wählen.\n");

		}//HilfeAusgeben

		/// <summary>
		/// Auswahl the des operanden.
		/// Eine Exception mit der Nachricht skip wird geworfen wenn die Operation verworfen werden soll.
		/// </summary>
		/// <returns>Der Operand für die Rechenoperation.</returns>
		static double AuswahlDesOperanden() {
			while ( true ) {
				Console.Write("Geben Sie den Operanden ein:");
				try {
					string Eingabe = Console.ReadLine();

					if(Eingabe == "x" ) {
						throw new Exception("Skip");
					}

					if(Eingabe == "h" ) {
						HilfeAusgeben();
						continue;
					}

					double retVal = Double.Parse(Eingabe);
					return retVal;

				} catch ( Exception e) {

					if(e.Message == "Skip" ) {
						throw new Exception("skip");
					} else {
						Console.WriteLine("Ungültige Eingabe bitte noch einmal probieren oder h für die hilfe eingeben.");
						continue;
					}

				}
			}
		}//AuswahlDesOperanden

		/// <summary>
		/// Auswahl the der rechenart.
		/// </summary>
		/// <returns>Gibt die Rechenart zurück. 0 Programm beenden, 1 +, 2 -, 3 /, 4 *</returns>
		static int AuswahlDerRechenart() {
			while ( true ) {
				Console.Write("Geben Sie eine Grundrechenart ein:");
				try {
					string Eingabe = Console.ReadLine();
					switch ( Eingabe ) {
						case ( "+" ):
							return 1;

						case ( "-" ):
							return 2;

						case ( "/" ):
							return 3;

						case ( "*" ):
							return 4;

						case ( "e" ):
							return 0;

						case ( "h" ):
							HilfeAusgeben();
							continue;

						default:
							Console.WriteLine("Ungültige Eingabe bitte noch einmal probieren oder h für die hilfe eingeben.\n");
							continue;
					}
				} catch ( Exception ) {
					Console.WriteLine("Ungültige Eingabe bitte noch einmal probieren oder h für die hilfe eingeben.\n");
					continue;
				}
			}
		}//AuswahlDerRechenart
	}
}
