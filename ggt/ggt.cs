﻿using System;

namespace nkrs14 {
	namespace ggt {
		class GrößterGemeinsamerTeiler {
			static void Main( string[] args ) {
				do {
					int Zahl1 = 3780;
					int Zahl2 = 3528;

					bool hasError = false;

					do {
						try {
							Console.Write("Geben sie die erste Zahl ein: ");
							string input = Console.ReadLine();

							if ( !input.Equals("Ende") ) {
								Zahl1 = Int32.Parse(input);
							} else {
								goto end;
							}

							hasError = false;
						} catch ( Exception ) {
							hasError = true;
							Console.WriteLine("\nEin Fehler ist aufgetreten. Bitte geben sie eine gültige Zahl oder \"Ende\" ein.\n");
						}

					} while ( hasError );

					do {
						try {
							Console.Write("Geben sie die zweite Zahl ein: ");
							string input = Console.ReadLine();

							if ( !input.Equals("Ende") ) {
								Zahl2 = Int32.Parse(input);
							} else {
								goto end;
							}

							hasError = false;
						} catch ( Exception ) {
							hasError = true;
							Console.WriteLine("\nEin Fehler is aufgetreten. Bitte geben sie eine gültige Zahl oder \"Ende\" ein.\n");
						} 

					} while ( hasError );

					int ggt = RecursiveGgt(Zahl1, Zahl2);
					Console.WriteLine("\nDer größte gemeinsame Teiler von " + Zahl1 + " und " + Zahl2 + " ist " + ggt + "\n\n");

				} while ( true );

			end:
				Console.WriteLine("Programm wird beendet.");
			}

			static int RecursiveGgt(int a, int b ) {
				if ( a < b ) {
					int temp = b;
					b = a;
					a = temp;
				}

				if ( b == 0 ) { return a; };
				if ( a == 0 ) { return b; };

				return RecursiveGgt(a-b , b);
			}
		}
	}
}