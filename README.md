# Aufgaben Objektorienentierte Programmierung

Dieses Repository enthält alle meine Lösungen für die Aufgaben zum Modul Objektorientierte Programmierung (PTB2).
Der Code ist mit monodeveop auf ubuntu 20.04 programmiert und getestet, sollte folglich auf allen Betriebssystemen laufen.
Es kann .net bzw .net core oder mono verwendet werden (zumindeest in der theorie).
