﻿using System;

namespace nkrs14.mittelwert_einfach {
	class Mittelwert_einfach {

		static void Main() {
			double[] messwerte;
			int anzahl = 0;

			do {
				try {
					Console.WriteLine("Anzahl der Messwerte:");
					string Eingabe = Console.ReadLine();

					anzahl = Int32.Parse(Eingabe);
				} catch (Exception) {
					anzahl = -1;
					Console.WriteLine("Ungültige eingabe bitte nocheinmal versuchen!\n\n");
				}

			} while ( anzahl < 1 );

			messwerte = new double[anzahl];
			double sum = 0d;

			for(int i = 0; i < anzahl; i++ ) {
				messwerte[i] = SafeDoubleInput("Bitte Messwert " + i + " eingeben:");
				sum += messwerte[i];
			}

			Console.WriteLine("Der Mittelwert der Messwerte beträgt " +  sum / anzahl );

		}

		static double SafeDoubleInput(string text) {
			while ( true ) {
				Console.WriteLine(text);

				try {
					string Eingabe = Console.ReadLine();

					double retval = Double.Parse(Eingabe);
					return retval;

				} catch ( Exception ) {
					Console.WriteLine("Ungültige eingabe bitte nocheinmal versuchen!\n\n");
				}
			}

		}
	}
}
